package com.service;

import io.prometheus.client.vertx.MetricsHandler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;

public class Boot {

  public static void main(String[] args) {
    final int port = getPort();
    final Vertx vertx = Vertx.vertx();
    final Router router = Router.router(vertx);
    router.route("/metrics").handler(new MetricsHandler());
    router.route().handler(BodyHandler.create());
    final HttpServer httpServer = vertx.createHttpServer();
    httpServer.requestHandler(request -> {
      final HttpServerResponse response = request.response();
      response.putHeader("content-type", "text/html");
      final String podName = System.getenv("THIS_POD_NAME");
      final String podNamespace = System.getenv("THIS_POD_NAMESPACE");
      final String podIp = System.getenv("THIS_POD_IP");
      final String content =
          "<body style=\"background-color:#6699CC;text-align:center\">" +
              String.format("<h1>POD Namespace: %s</h1>", podNamespace) +
              String.format("<h1>POD Name: %s</h1>", podName) +
              String.format("<h1>POD IP: %s</h1>", podIp) +
          "</body>";
      response.end(content);
    });
    httpServer.listen(port, handler -> {
      if (handler.succeeded()) {
        System.out.println("Listening at port " + port);
      } else {
        System.err.println("Failed to listen on port " + port);
      }
    });
  }

  private static int getPort() {
    final String port = System.getenv("PORT");
    if (port == null) {
      throw new IllegalArgumentException("Missing environment variable: PORT");
    }
    return Integer.parseInt(port);
  }

}
