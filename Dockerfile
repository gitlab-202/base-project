FROM openjdk:alpine

COPY /build/libs/service.jar /home/

EXPOSE 8080

CMD ["java", "-jar", "/home/service.jar"]
